package machine

import java.util.*

enum class StatePlayground {
    RUN,
    STOP
}

enum class CoffeeMachineState {
    CHOOSE_MAIN_OPERATION,
    CHOOSE_BUY_COFFEE,
    FILL_WATER,
    FILL_MILK,
    FILL_COFFEE,
    FILL_CUPS
}

enum class MainOperation(val operation: String) {
    BUY("buy"),
    FILL("fill"),
    REMAIN("remaining"),
    TAKE("take"),
    EXIT("exit")
}

class Drink(val water: Int = 0, val milk: Int = 0, val coffee: Int = 0, val money: Int = 0, val name: String = "unknown")

class CoffeeMachine {
    var state: CoffeeMachineState = CoffeeMachineState.CHOOSE_MAIN_OPERATION

    var water: Int = 400
    var milk: Int = 540
    var coffee: Int = 120
    var cups: Int = 9
    var money: Int = 550

    companion object {
        object Recipes {
            val espresso = Drink(water = 250, coffee = 16, money = 4, name = "espresso")
            val latte = Drink(water = 350, milk = 75, coffee = 20, money = 7, name = "latte")
            val cappuccino = Drink(water = 200, milk = 100, coffee = 12, money = 6, name = "cappuccino")
        }

    }

    private fun printState() {
        println("\nThe coffee machine has:")
        println("$water of water")
        println("$milk of milk")
        println("$coffee of coffee beans")
        println("$cups of disposable cups")
        println("$money of money\n")
    }

    private fun moveToState(nextState: CoffeeMachineState) {
        state = nextState
    }

    private fun fillWater(count: Int) {
        water += count

        moveToState(CoffeeMachineState.FILL_MILK)
    }

    private fun fillMilk(count: Int) {
        milk += count

        moveToState(CoffeeMachineState.FILL_COFFEE)
    }

    private fun fillCoffee(count: Int) {
        coffee += count

        moveToState(CoffeeMachineState.FILL_CUPS)
    }

    private fun fillCups(count: Int) {
        cups += count

        moveToState(CoffeeMachineState.CHOOSE_MAIN_OPERATION)
    }

    private fun makeCoffee(subject: Int) {
        val recipe = when(subject) {
            1 -> Recipes.espresso
            2 -> Recipes.latte
            3 -> Recipes.cappuccino
            else -> Drink()
        }

        if (recipe.name == "unknown") {
            println("Unknown recipe $subject")
            state = CoffeeMachineState.CHOOSE_MAIN_OPERATION
            return
        }

        if (water < recipe.water) {
            println("Sorry, not enough water!")
            state = CoffeeMachineState.CHOOSE_MAIN_OPERATION
            return
        }

        if (coffee < recipe.coffee) {
            println("Sorry, not enough coffee!")
            state = CoffeeMachineState.CHOOSE_MAIN_OPERATION
            return
        }

        if (milk < recipe.milk) {
            println("Sorry, not enough milk!")
            state = CoffeeMachineState.CHOOSE_MAIN_OPERATION
            return
        }

        if (cups == 0) {
            println("Sorry, not enough cups!")
            state = CoffeeMachineState.CHOOSE_MAIN_OPERATION
            return
        }

        println("I have enough resources, making you a coffee!")

        milk -= recipe.milk
        water -= recipe.water
        coffee -= recipe.coffee
        money += recipe.money
        cups -= 1

        moveToState(CoffeeMachineState.CHOOSE_MAIN_OPERATION)
    }

    private fun takeOperation() {
        println("I gave you $money")
        money = 0
        moveToState(CoffeeMachineState.CHOOSE_MAIN_OPERATION)
    }

    fun processCommand(command: String): Boolean {
        when(state) {
            CoffeeMachineState.CHOOSE_MAIN_OPERATION -> when(command) {
                MainOperation.EXIT.operation -> return false
                MainOperation.BUY.operation -> moveToState(CoffeeMachineState.CHOOSE_BUY_COFFEE)
                MainOperation.FILL.operation -> moveToState(CoffeeMachineState.FILL_WATER)
                MainOperation.REMAIN.operation -> printState()
                MainOperation.TAKE.operation -> takeOperation()
            }
            CoffeeMachineState.CHOOSE_BUY_COFFEE -> when(command) {
                "back" -> moveToState(CoffeeMachineState.CHOOSE_MAIN_OPERATION)
                else -> makeCoffee(command.toInt())
            }
            CoffeeMachineState.FILL_WATER -> fillWater(command.toInt())
            CoffeeMachineState.FILL_MILK -> fillMilk(command.toInt())
            CoffeeMachineState.FILL_COFFEE -> fillCoffee(command.toInt())
            CoffeeMachineState.FILL_CUPS -> fillCups(command.toInt())
        }

        return true
    }

    fun alert() {
        println(when(state) {
            CoffeeMachineState.CHOOSE_MAIN_OPERATION -> "Write action (buy, fill, take, remaining, exit)"
            CoffeeMachineState.CHOOSE_BUY_COFFEE -> "What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, back - to main menu:"
            CoffeeMachineState.FILL_WATER -> "Write how many ml of water do you want to add:"
            CoffeeMachineState.FILL_MILK -> "Write how many ml of milk do you want to add:"
            CoffeeMachineState.FILL_COFFEE -> "Write how many grams of coffee beans do you want to add:"
            CoffeeMachineState.FILL_CUPS -> "Write how many disposable cups of coffee do you want to add:"
        })
    }
}

object Playground {
    private var state: StatePlayground = StatePlayground.STOP
    private val coffeeMachine: CoffeeMachine = CoffeeMachine()

    fun run() {
        if (state == StatePlayground.RUN) {
            println("Playground already started")
            return;
        }

        val scanner = Scanner(System.`in`)
        state = StatePlayground.RUN

        while(state == StatePlayground.RUN) {
            coffeeMachine.alert()
            val command = scanner.next()
            if(!coffeeMachine.processCommand(command)) {
                state = StatePlayground.STOP
            }
        }
    }

    fun stop() {
        state = StatePlayground.STOP
    }
}

fun main() {
    Playground.run()
}